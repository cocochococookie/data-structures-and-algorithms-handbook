# Table of Contents

* [Introduction](README.md)

## Meet the Team

* [BSIS 2](team/bsis_2.md)
* [ACT 2](team/act_2.md)

## Exploring the subject

* [Links and Resources](subject_exploration/links_and_resources.md)
* [Notes](subject_exploration/notes.md)
